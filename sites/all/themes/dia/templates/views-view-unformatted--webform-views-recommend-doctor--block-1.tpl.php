<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div <?php if ($classes_array[$id]) { print 'class="' . $classes_array[$id] .'"';  } ?>>
    <?php $nodeid = strip_tags($row);
    	  $nid = trim($nodeid);
    	  $node = node_load($nid);
    	  $name = "";
    	  
    	  if(isset($node->field_spacialist_in['und'])) {
    	    foreach($node->field_spacialist_in['und'] as $key=>$val) {
    	  	  $tids[] = $val['tid']; 
	        }
	        $terms = taxonomy_term_load_multiple($tids);
		    foreach ($terms as $term) {
  			  $name .= $term->name."-";
		    }
		    $speciality = "<div class='views-field views-field-field-location'><div class='field-content'>".rtrim($name, "-")."</div></div>";	
	      }

		  $content = "<div class='views-field views-field-title'><span class='field-content'><a href='node/".$nid."'>".$node->title."</a></span></div>".$speciality;
    	  print $content; ?>
  </div>
<?php endforeach; ?>
